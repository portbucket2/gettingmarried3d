﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [Header("Start Panel")] public TextMeshProUGUI levelNumberText;
    public Button tapButton;
    public Animator startPanelAnimator;
    [Space(15)]
    public UnityEvent OnStartButtonPressedEvent;
    
    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    private static UIManager m_Instance;
    
    [Header("Upper panel")] public Animator upperPanelAnimator;

    

    public TextMeshProUGUI currentLevelText;
    
    
    [Header("Level Complete")] public Animator levelCompleteAnimator;
    public Button collectButton;
    public ParticleSystem levelCompleteParticle;


    [Header("Buttons")]
    public GameObject buttonPanel;
    public Button acceptButton;
    public Button rejectButton;
    public Button matchNowButton;

    public Button inventoryButton;
    public bool isFromInventoryButton;

    public List<ScrollItem> scitemList;

    public Scrollbar scrollbar;
    public Button leftButton;
    public Button rightButton;


    public static UIManager Instance
    {
        get { return m_Instance; }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = this;
        }
    }

    public void DeactivateSaveButton()
    {
        acceptButton.interactable = false;
    }
    public void ActivateSaveButton()
    {
        acceptButton.interactable = true;
    }

    internal void ResetEntry()
    {
       // startPanelAnimator.SetTrigger(EXIT);
        upperPanelAnimator.SetTrigger(ENTRY);
        startPanelAnimator.gameObject.SetActive(false);
        ActivateInventoryButton();
        //upperPanelAnimator.gameObject.SetActive(false);
    }

    private void Start()
    {
        currentLevelText.text = "Level " +LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        ButtonInteraction();
    }
    public void LoadLevelNumber()
    {
        currentLevelText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
        levelNumberText.text = "Level " + LevelManager.Instance.gameCurrentLevel.ToString();
    }


    private void ButtonInteraction()
    {
        tapButton.onClick.AddListener(delegate
        {
            startPanelAnimator.SetTrigger(EXIT);
            upperPanelAnimator.SetTrigger(ENTRY);
            ActivateInventoryButton();
            OnStartButtonPressedEvent.Invoke();
        });



        acceptButton.onClick.AddListener(delegate
        {
            AnalyticsAssitantGM.ReportSaveForLater(LevelManager.Instance.gameCurrentLevel);

            EntryHallManager.Instance.AcceptClient();
            buttonPanel.SetActive(false);


        });
        rejectButton.onClick.AddListener(delegate
        {
            AnalyticsAssitantGM.ReportReject(LevelManager.Instance.gameCurrentLevel);

            EntryHallManager.Instance.RejectClient();
            buttonPanel.SetActive(false);
        });
        matchNowButton.onClick.AddListener(delegate
        {
            AnalyticsAssitantGM.ReportFindMatch(LevelManager.Instance.gameCurrentLevel);

            EntryHallManager.Instance.MatchNow();
        });

        inventoryButton.onClick.AddListener(delegate
        {
        isFromInventoryButton = true;
        DeactivateInventoryButton();
        DeactivateLevelText();

        DataManager.Instance.LoadData();

        InventoryManager.Instance.ActivateInventory();
        InventoryManager.Instance.SetAllSlotWithSavedList(DataManager.Instance.Inventory.GetSavedSlotList());

        List<ClientProfile> clientProfiles = DataManager.Instance.GetAllSavedClientProfiles();
        if (clientProfiles.Count >0)
            {
            InventoryManager.Instance.HighlightCurrentSlot(clientProfiles[0]);
            }

            StatsUI.Instance.DeactivateStatsPanel();
            UIManager.Instance.DeactivateDecisionPanel();

            GameStateManager.RequestSwitch(GameState.Inventory);
        });
        leftButton.onClick.AddListener(delegate
        {
            //scrollbar.size -= 0.45f;
            StartCoroutine(waitAndLerp(false));
        });
        rightButton.onClick.AddListener(delegate
        {
           // scrollbar.size += 0.45f;
            StartCoroutine(waitAndLerp(true));
        });
    }

    IEnumerator waitAndLerp(bool isPlus)
    {
        if (isPlus)
        {
            float val = scrollbar.size + 0.46f;
            float tVal = 0f;
        while (scrollbar.size<val)
            {
                tVal += Time.deltaTime;
                scrollbar.size = Mathf.Lerp(scrollbar.size, val,tVal);
                yield return new WaitForEndOfFrame();
            }
        }
        else
        {
            float val = scrollbar.size - 0.46f;
            float tVal = 0f;
            while (scrollbar.size>val)
            {
                tVal += Time.deltaTime;
                scrollbar.size = Mathf.Lerp(scrollbar.size, val, tVal);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    public void SetScrollSlotWithSavedList(List<Slot> saveSlot)
    {
        for (int i = 0; i < scitemList.Count; i++)
        {
            scitemList[i].Deactive();
        }
        for (int i = 0; i < saveSlot.Count; i++)
        {
            scitemList[i].Activate();
            Slot slot = saveSlot[i];
            ClientProfile clientProfile = GameDataReader.Instance.GetClientProfileWithID(slot.savedProfileID);
            scitemList[i].SetInfo(clientProfile,this);
        }

        if (saveSlot.Count == 2)
        {
            scrollbar.size = 0.45f;
        }
        else if (saveSlot.Count == 3)
        {
            scrollbar.size = 0.55f;
        }else if( saveSlot.Count == 4)
        {
            scrollbar.size = 0.49f;
        }else if(saveSlot.Count == 5)
        {
            scrollbar.size = 0.25f;
        }
        else if(saveSlot.Count == 6)
        {
            scrollbar.size = 0.05f;
        }
       
    }

    public void ClickButton(ClientProfile profile)
    {
        Debug.Log(profile.clientInformation.title);
        StartCoroutine(WaitandGo(profile));
        LevelManager.Instance.IncreaseGameLevel();
    }
    IEnumerator WaitandGo(ClientProfile profile)
    {
        DataManager.Instance.SetInvenotrySelectedProfile(Gameplay.Instance.currentLevelData.clientProfile);
        yield return new WaitForEndOfFrame();
        DataManager.Instance.SetInventoryProfileList(DataManager.Instance.GetAllSavedClientProfiles());
        yield return new WaitForEndOfFrame();
        DataManager.Instance.scrollSelectProfile = profile;
        DataManager.Instance.isScrollMatch = true;
        
        yield return new WaitForEndOfFrame();
        Gameplay.Instance.GotoMatchmakingScene();
    }

    public void ShowLevelComplete()
    {
        levelCompleteAnimator.SetTrigger(ENTRY);
        
        levelCompleteParticle.Play();
        

        collectButton.onClick.AddListener(delegate
        {
            
        });
    }

    public void ActivateDecisionPanel()
    {
        buttonPanel.SetActive(true);
        SetScrollSlotWithSavedList(DataManager.Instance.Inventory.GetSavedSlotList());
    }

    public void DeactivateDecisionPanel()
    {
        buttonPanel.SetActive(false);
    }

    public void ActivateInventoryButton()
    {
        if (DataManager.Instance.GetAllSavedClientProfiles().Count == 0)
        {
            inventoryButton.interactable = false;
        }
        else
        {
            inventoryButton.interactable = true;
        }
        inventoryButton.GetComponent<Animator>().SetTrigger(ENTRY);
    }
    public void DeactivateInventoryButton()
    {
        //inventoryButton.gameObject.SetActive(false);
        inventoryButton.GetComponent<Animator>().SetTrigger(EXIT);
    }
    public void ActivateLevelText()
    {
        currentLevelText.gameObject.SetActive(true);
    }
    public void DeactivateLevelText()
    {
        currentLevelText.gameObject.SetActive(false);
    }

    public void UpdateUI()
    {
        throw new NotImplementedException();
    }
}
