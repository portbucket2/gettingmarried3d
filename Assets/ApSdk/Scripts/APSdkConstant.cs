﻿namespace APSdk
{
    public static class APSdkConstant
    {

        public const string NameOfSDK = "APSdk";

        public const int EXECUTION_ORDER_LionKitWrapper = -1000;
        public const int EXECUTION_ORDER_FacebookWrapper= -900;
        public const int EXECUTION_ORDER_AdjustWrapper  = -800;
        public const int EXECUTION_ORDER_GameAnalytics  = -700;
        public const int EXECUTION_ORDER_Firebase       = -600;
        

        public const string APSdk_LionKit = "APSdk_LionKit";
        public const string APSdk_Facebook = "APSdk_Facebook";
        public const string APSdk_Adjust = "APSdk_Adjust";
        public const string APSdk_GameAnalytics = "APSdk_GameAnalytics";
        public const string APSdk_Firebase = "APSdk_Firebase";
    }
}

