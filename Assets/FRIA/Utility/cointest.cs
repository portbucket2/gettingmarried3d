﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cointest : MonoBehaviour
{
    public Text t1;
    public Text t2;
    public static CurrencyManager<Coins> curencyMan = new CurrencyManager<Coins>();
    // Start is called before the first frame update
    void Start()
    {

        curencyMan.ChangeBy(Coins.Coin0, 25);

        curencyMan.AddListner_BalanceChanged(Coins.Coin0, () => 
        {
            t1.text = curencyMan.GetBalance(Coins.Coin0).ToString(); 
        });
        curencyMan.AddListner_BalanceChanged(Coins.Coin1, () =>
        {
            t2.text = curencyMan.GetBalance(Coins.Coin1).ToString();
        });
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {

            curencyMan.ChangeBy(Coins.Coin0, 25);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {

            curencyMan.ChangeBy(Coins.Coin0, -25);
        }


        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {

            curencyMan.ChangeBy(Coins.Coin1, -10);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {

            curencyMan.ChangeBy(Coins.Coin1, 10);
        }

    }
}
public enum Coins
{
    Coin0 = 0,
    Coin1 = 1,
}