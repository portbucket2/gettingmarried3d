﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

#if UNITY_EDITOR
using UnityEditor;
#endif
public class CaptureScreenShot : MonoBehaviour
{

    public KeyCode key = KeyCode.Insert;
    public string fileName;

    public HardData<int> countHD;

    public int count 
    {
        get
        {
            if (countHD == null)
            {
                countHD = new HardData<int>("FRIA_SCREEN_CAPTURE_COUNT",0);
            }
            return countHD.value;
        }
        set
        {
            if (countHD == null)
            {
                countHD = new HardData<int>("FRIA_SCREEN_CAPTURE_COUNT", 0);
            }
            countHD.value = value;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            Capture();
        }
    }
    public void Capture()
    {
        ScreenCapture.CaptureScreenshot(fileName + count++ + ".png");
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(CaptureScreenShot))]
public class CaptureScreenShotEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        CaptureScreenShot css = (CaptureScreenShot)target;
        if (GUILayout.Button("Refresh", GUILayout.Height(35)))
        {
            css.count = 0;
        }
        if (GUILayout.Button("Capture", GUILayout.Height(60)))
        {
            css.Capture();
        }
    }
}
#endif