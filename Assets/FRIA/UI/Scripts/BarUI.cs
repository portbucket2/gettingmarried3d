﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BarUI : MonoBehaviour
{
    [Header("Behaviour")]
    public bool log = false;
    [Range(0, 5000)]
    public float perCentPerSec = 100;
    public float textUpdateInterval = 0.1f;

    [Header("Style")]
    public BarTextStyle textStyle = BarTextStyle.NONE;
    public Image.FillMethod fillMethod = Image.FillMethod.Horizontal;
    public int fillOrigin = 0;

    [Header("References")]
    public Image barImage;
    public Text barText;
    GameObject barTextObj;

    private void Start()
    {
        barImage.type = Image.Type.Filled;
        barImage.fillMethod = fillMethod;
        barImage.fillOrigin = fillOrigin;

        barTextObj = barText.gameObject;
    }

    Coroutine runningRoutine;

    public void LoadValue(float targetValue, float maxValue, bool instant = false,  bool textRounding = true, System.Action onExpectedToBeDone = null)
    {
        if (!barTextObj) barTextObj = barText.gameObject;

        if (instant || targetValue<valueVisible)
        {
            valueTarget = targetValue;
            valueMax = maxValue;

            barImage.fillAmount = targetValue / maxValue;
            UpdateText(targetValue, maxValue, textRounding);

            valueVisible = targetValue;

            if (runningRoutine != null) { 
            
                StopCoroutine(runningRoutine);
                runningRoutine = null;
            }
            onExpectedToBeDone?.Invoke();
        }
        else
        {
            if (valueVisible < valueTarget)
            {
                float workP = Mathf.Clamp01((Time.time - startTime) / timeSpan);

                valueVisible = Mathf.Lerp(valueInitial, valueTarget, workP);
                barImage.fillAmount = valueVisible / valueMax;

                if (Time.time > lastTextUpdateTime + textUpdateInterval)
                {
                    UpdateText(valueVisible, valueMax, textRounding);
                    lastTextUpdateTime = Time.time;
                }
            }
            valueTarget = targetValue;
            valueMax = maxValue;

            valueInitial = valueVisible;
            float progVisible = valueVisible / valueMax;
            float progTarget = valueTarget / valueMax;
            timeSpan = (progTarget - progVisible) * 100 / perCentPerSec;
            startTime = Time.time;
            lastTextUpdateTime = -100000;
            if(onExpectedToBeDone!=null)FRIA.Centralizer.Add_DelayedMonoAct(this, onExpectedToBeDone, timeSpan + 0.01f);

            if (runningRoutine == null) runningRoutine = StartCoroutine(ProgressRoutine(textRounding));
        }
    }
    float valueInitial;
    float lastTextUpdateTime;
    float valueVisible; 
    float valueTarget; 
    float valueMax;
    float timeSpan;
    float startTime;
     IEnumerator ProgressRoutine(bool textRounding )
    {
        while (Time.time< startTime+timeSpan)
        {
            //if (log) Debug.Log("c");
            float workP = Mathf.Clamp01((Time.time - startTime) / timeSpan);

            valueVisible = Mathf.Lerp(valueInitial,valueTarget,workP);
            barImage.fillAmount = valueVisible / valueMax;
            //Debug.Log(valueVisible);
            if (Time.time > lastTextUpdateTime + textUpdateInterval)
            {
                UpdateText(valueVisible, valueMax, textRounding);
                lastTextUpdateTime = Time.time;
            }
            yield return null;
        }
        valueVisible = valueTarget;
        barImage.fillAmount = valueVisible / valueMax;
        UpdateText(valueVisible, valueMax, textRounding);
        runningRoutine = null;
    }

    void UpdateText(float visibleProgress, float maxProgress, bool rounding = true)
    {
        if (rounding)
        {
            visibleProgress = Mathf.Round(visibleProgress);
            maxProgress = Mathf.Round(maxProgress);
        }
        switch (textStyle)
        {
            case BarTextStyle.NONE:
                {
                    if (barTextObj.activeSelf) barTextObj.SetActive(false);
                }
                break;
            case BarTextStyle.NUMBER:
                if (!barTextObj.activeSelf) barTextObj.SetActive(true);
                barText.text = visibleProgress.ToString();
                break;
            case BarTextStyle.RATIO:
                if (!barTextObj.activeSelf) barTextObj.SetActive(true);
                barText.text = string.Format("{0}/{1}", visibleProgress,maxProgress);
                break;
            default:
                throw new System.Exception("Undefined style");
        }
    }

    public enum BarTextStyle
    {
        NONE = 0,
        NUMBER = 1,
        RATIO = 2,
    }
}
