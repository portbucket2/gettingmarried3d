﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HDRColorBlink : MonoBehaviour
{
    public List<Renderer> renderers;
    public Color highColor;
    public Color lowColor;
    public float period = 2;


    // Update is called once per frame
    void Update()
    {

        int wc = (int) ( Time.time /period);

        float phase = (Time.time / period) - wc;
        float lerpCoef;
        if (phase < 0.5f)
        {
            lerpCoef = phase * 2; 
        }
        else
        {
            lerpCoef = (1 - phase) * 2;
        }

        foreach (Renderer rend in renderers)
        {
            rend.material.SetColor("_EmissionColor",Color.Lerp(highColor, lowColor, lerpCoef));
        }

    }
}
