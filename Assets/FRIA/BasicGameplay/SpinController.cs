﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinController : MonoBehaviour
{
    public Vector3 spinRate = new Vector3(0,1,0);

    private void Update()
    {
        transform.Rotate(spinRate * Time.deltaTime);
    }

}
