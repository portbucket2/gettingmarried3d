﻿using UnityEngine;
using FRIA;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
public class FolderSpecificOperationWindow : EditorWindow
{
    string folderPath;
    [MenuItem("FRIA/Windows/FolderSpecificOperationWindow")]
    static void Init()
    {
        FolderSpecificOperationWindow window = (FolderSpecificOperationWindow)EditorWindow.GetWindow(typeof(FolderSpecificOperationWindow));

        window.Show();
    }



    void OnGUI()
    {
        GUILayout.Space(3);

        string[] guids = AssetDatabase.FindAssets("FolderSpecificOperationWindow t:Script");
        string path = AssetDatabase.GUIDToAssetPath(guids[0]);
        Object obj = AssetDatabase.LoadAssetAtPath<Object>(path);
        EditorGUI.BeginDisabledGroup(true);
        EditorGUILayout.ObjectField("Script:", obj, typeof(Object), false);
        EditorGUI.EndDisabledGroup();
        folderPath = EditorGUILayout.TextField("Folder Path", folderPath);

        GUILayout.Space(5);
        if (GUILayout.Button("Job 0", GUILayout.Height(35)))
        {
            Job_0();
        }
    }



    void Job_0()
    {
        string[] folderPaths = new string[1];
        folderPaths[0] = folderPath;
        string[] guids = AssetDatabase.FindAssets("t:Material", folderPaths);
        int count = 0;
        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            Material m = AssetDatabase.LoadAssetAtPath<Material>(path);
            //float metallic = m.GetFloat("_Metallic");
            //if (metallic < 0.01f)
            //{
            //    Debug.Log(string.Format("{0} -> {1}", path, m.shader.name),m);
            //    if (m.IsKeywordEnabled("_EMISSION"))
            //    {
            //        m.shader = Shader.Find("FRIA/SuperLit/SuperLit_TexColorEmissive");
            //    }
            //    else
            //    {

            //        m.shader = Shader.Find("FRIA/SuperLit/SuperLit_TexColor");
            //    }
            //    EditorFix.SetObjectDirty(m);
            //}
            //else
            //{
            //    Debug.LogError(string.Format("{0} -> mettalic", path),m);
            //}
            if (m.shader.name == "FRIA/SuperLit/SuperLit_TexColor")
            {
                count++;
                m.SetFloat("_BrightnessOffset", 0.8f);
            }
            

        }
        Debug.LogFormat("{0}/{1}",count,guids.Length);

        AssetDatabase.SaveAssets();
        //AssetDatabase.Refresh();
    }
  

}
#endif