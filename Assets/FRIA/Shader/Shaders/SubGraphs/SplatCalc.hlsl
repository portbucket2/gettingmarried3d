
void SplatCalc_half(half3 a, half3 b, half3 c, half3 d, half3 e, half3 f, half3 g, half3 h,
    half4 t1, half4 t2,
    out half3 col, out half sum, out half maxVal)
{

            float p = 100;
            float offset = 0.135f;

            int w1 = 1;
            int w2 = 1;
            int w3 = 1;
            int w4 = 1;
            float val1 = t1.g;
            float val2 = t1.a;
            float val3 = t2.g;
            float val4 = t2.a;
            if (t1.r > t1.g)
            {
                w1 = 0;
                val1 = t1.r;
            }
            if (t1.b > t1.a)
            {
                w2 = 0;
                val2 = t1.b;
            }
            if (t2.r > t2.g)
            {
                w3 = 0;
                val3 = t2.r;
            }
            if (t2.b > t2.a)
            {
                w4 = 0;
                val4 = t2.b;
            }

            int ww1 = 1;
            int ww2 = 1;
            float vv1 = val2;
            float vv2 = val4;
            if (val1 > val2)
            {
                ww1 = 0;
                vv1 = val1;
            }
            if (val3 > val4)
            {
                ww2 = 0;
                vv2 = val3;
            }
            int www = 1;
            float vvv = vv2;
            if (vv1 > vv2)
            {
                www = 0;
                vvv = vv1;
            }
            maxVal = vvv;

            float sigV = ((1.0 / (1.0 + (exp(-(vvv - offset) * 100)))));
            float sigVR = 1-sigV;

            float t1r = t1.r;
            float t1g = t1.g;
            float t1b = t1.b;
            float t1a = t1.a;
            float t2r = t2.r;
            float t2g = t2.g;
            float t2b = t2.b;
            float t2a = t2.a;

            t1.r = t1.r * sigVR;
            t1.g = t1.g * sigVR;
            t1.b = t1.b * sigVR;
            t1.a = t1.a * sigVR;          
            t2.r = t2.r * sigVR;
            t2.g = t2.g * sigVR;
            t2.b = t2.b * sigVR;
            t2.a = t2.a * sigVR;

            float m = sigV;// / sigVR;
            int windex = 0;
            if (www == 0)
            {
                if (ww1 == 0)
                {
                    if (w1 == 0)
                    {
                        t1.r = t1r * m;
                    }
                    else
                    {
                        t1.g = t1g * m;
                    }
                }
                else
                {
                    if (w2 == 0)
                    {
                        t1.b = t1b * m;
                    }
                    else
                    {
                        t1.a = t1a * m;
                    }
                }
            }
            else
            {
                if (ww2 == 0)
                {
                    if (w3 == 0)
                    {
                        t2.r = t2r * m;
                    }
                    else
                    {
                        t2.g = t2g * m;
                    }
                }
                else
                {
                    if (w4 == 0)
                    {
                        t2.b = t2b * m;
                    }
                    else
                    {
                        t2.a = t2a * m;
                    }
                }
            }

            //t1.r = ((1.0 / (1.0 + (exp(-(t1.r - offset) * 100)))));
            //t1.g = ((1.0 / (1.0 + (exp(-(t1.g - offset) * 100)))));
            //t1.b = ((1.0 / (1.0 + (exp(-(t1.b - offset) * 100)))));
            //t1.a = ((1.0 / (1.0 + (exp(-(t1.a - offset) * 100)))));


            //t2.r = ((1.0 / (1.0 + (exp(-(t2.r - offset) * 100)))));
            //t2.g = ((1.0 / (1.0 + (exp(-(t2.g - offset) * 100)))));
            //t2.b = ((1.0 / (1.0 + (exp(-(t2.b - offset) * 100)))));
            //t2.a = ((1.0 / (1.0 + (exp(-(t2.a - offset) * 100)))));



            sum = t1.r + t1.g + t1.b + t1.a + t2.r + t2.g + t2.b + t2.a;
            if (sum == 0)
            {
                sum = 0.001;
            }

            col = t1.r * a + t1.g * b + t1.b * c + t1.a * d + t2.r * e + t2.g * f + t2.b * g + t2.a * h;;
            col = col / sum;


}