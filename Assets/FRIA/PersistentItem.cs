﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentItem : MonoBehaviour
{
    public static Dictionary<string, PersistentItem> dic = new Dictionary<string, PersistentItem>();
    public string persistenceID = System.DateTime.Now.Ticks.ToString();

    void Awake()
    {        
        if (dic.ContainsKey(persistenceID))
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            dic.Add(persistenceID,this);
        }
    }

    public static void ActivateChildren(string id, bool active, bool warning = true)
    {
        if (dic.ContainsKey(id))
        {
            foreach (Transform child in dic[id].transform)
            {
                child.gameObject.SetActive(active);
            }
        }
        else 
        {
           if(warning) Debug.LogError("Item NotFound");
        }
    }
}
