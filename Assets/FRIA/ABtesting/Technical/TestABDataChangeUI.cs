﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TestABDataChangeUI : MonoBehaviour
{
    public Text typeTitleText;
    public Text valueText;
    public Button nextValueButton;
    public Button prevValueButton;

    int selectionIndex = -1;

    string[] values;
    string[] titles;
    public bool loaded = false;

    ABManager.HardAB ab;

    public void Load(string abkey, string[] values=null, string[] titles =null)
    {
        if (values == null)
        {
            values = new string[] { "0", "1" };
            titles = new string[] { "disabled", "enabled" };
        }
        if (titles == null)
        {
            titles = new string[values.Length];
            for (int i = 0; i < values.Length; i++)
            {
                titles[i] = values[i];
            }
        }
        loaded = true;
        this.values = values;
        this.titles = titles;
        ab = ABManager.GetABAccess(abkey);

        for (int i = 0; i < values.Length; i++)
        {
            if (values[i] == ab.GetValue()) selectionIndex = i;
        }


        typeTitleText.text = ab.GetID();
        if (selectionIndex >= 0)
        {
            valueText.text = titles[selectionIndex];
        }
        else
        {
            valueText.text = "server was silent";
        }

        nextValueButton.onClick.RemoveAllListeners();
        nextValueButton.onClick.AddListener(OnNext);

        prevValueButton.onClick.RemoveAllListeners();
        prevValueButton.onClick.AddListener(OnPrev);
    }

    void OnNext()
    {
        if (selectionIndex == -1) selectionIndex = 0;
        else
        {
            selectionIndex++;
            if (selectionIndex >= values.Length) selectionIndex = 0;
        }
        valueText.text = titles[selectionIndex];
    }
    void OnPrev()
    {
        if (selectionIndex == -1) selectionIndex = 0;
        else
        {
            selectionIndex--;
            if (selectionIndex < 0) selectionIndex = values.Length - 1;
        }

        valueText.text = titles[selectionIndex];
    }

    public void OnApply()
    {
        if (selectionIndex >= 0)
        {
            ab.ForceSetValue(values[selectionIndex]);
        }
    }
}
