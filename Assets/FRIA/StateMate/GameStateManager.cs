﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;

public class GameStateManager : MonoBehaviour
{
    public static GameStateManager Instance { get; private set; }

    public static IStateMateAccess<GameState> stateAccess { get { return Instance.stateMate; } }

    public static GameState GetCurrentState { get { return stateAccess.CurrentState; } }

    public static bool isBaseLoop { get { return GetCurrentState == GameState.BaseLoop; } }
    
    private StateMate<GameState> stateMate;

    //public List<GameObject> startObjs;
    //public List<GameObject> purchaseObjs;
    //public List<GameObject> inGameObjs;
    private void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            Init();
        }
    }
    private void Init()
    {
        stateMate = new StateMate<GameState>(initial: GameState.Init, enableDefinedTransitionOnly: true, loadEmpties: true);

        //stateMate.AddStateWithEnableList(GameState.Init,startObjs);
        //stateMate.AddStateWithEnableList(GameState.Purchase, purchaseObjs);
        //stateMate.AddStateWithEnableList(GameState.Action, inGameObjs);

        stateMate.AddTransition(GameState.Init, GameState.BaseLoop);
        stateMate.AddTransition(GameState.BaseLoop, GameState.Inventory);
        stateMate.AddTransition(GameState.BaseLoop, GameState.Matchmaking);
        stateMate.AddTransition(GameState.Inventory, GameState.Matchmaking);
        stateMate.AddTransition(GameState.Inventory, GameState.BaseLoop);

        stateMate.AddTransition(GameState.Matchmaking, GameState.Result);
        stateMate.AddTransition(GameState.Matchmaking, GameState.Inventory);
        stateMate.AddTransition(GameState.Result, GameState.BaseLoop);


        //stateMate.AddTranstionCallback(GameState.Action, GameState.Fail,Test);
        // stateMate.AddStateEntryCallback(GameState.Fail, Test);

        //stateAccess.AddStateEntryCallback(GameState.Fail, Test);
        stateMate.logStateSwitches = true;
    }

    private void Test()
    {
        throw new NotImplementedException();
    }

    private void Start()
    {
        stateMate.SetInitialSwitches();
    }
    public static void RequestSwitch(GameState gs)
    {
        Instance.stateMate.SwitchState(gs);
    }
}

public enum GameState
{
    Init = 0,
    BaseLoop = 1,
    Inventory = 2,
    Matchmaking = 3,
    Result = 4
}