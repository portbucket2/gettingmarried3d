﻿using UnityEngine;

[System.Serializable]
public class ClientProfile
{
    public string name;
    public int clientID;
    public ClientInfo clientInformation;
    public ClientInfo clientRequirement;
    public int initialCurrency;
    public int visualId;

    public ClientProfile()
    {
        clientInformation = new ClientInfo();
        clientRequirement = new ClientInfo();
    }
}

//[System.Serializable]
//public class ClientInfo
//{
//    public int id;
//    public string title;
//    public Gender gender;
//    public Profession profession;
//    public int height;
//    public int age;
//    public int weight;
//}

[System.Serializable]
public enum Gender
{
    NONE = 0,
    Male = 1,
    Female =2
}
//[System.Serializable]
//public enum Profession
//{
//    NONE =0,
//    Athelete =1,
//    Journalist=2,
//    Doctor=3,
//    Author=4
//}

[System.Serializable]
public class Slot
{
    public int slotID;
    public int savedProfileID;
    public int startingTurn;
    public bool isSlotEmpty;
    public int initialCurrency;
}

[System.Serializable]
public class MovePoint
{
    public int pointID;
    public Transform pointTrans;
    public bool isFill;
}
[System.Serializable]
public class ClientRef
{
    public int clientID;
    public GameObject clientObject;
    public ClientAnimationController clientController;
}
[System.Serializable]
public class LevelData
{
    public int id;
    public ClientProfile clientProfile;

    public LevelData()
    {
        clientProfile = new ClientProfile();
    }
    public LevelData(int t_CurrentLevel)
    {
        SequenceClass sequenceClass = new SequenceClass(); 
        if (DataManager.Instance.isInventoryPopulate)
        {
            sequenceClass = GameDataReader.Instance.sequenceClassList[t_CurrentLevel + 2];
        }
        else
        {
            sequenceClass = GameDataReader.Instance.sequenceClassList[t_CurrentLevel -1];
        }
        id = sequenceClass.id;

        ClientProfile currentProfile = new ClientProfile();
        currentProfile =  GameDataReader.Instance.GetClientProfile(id);
        currentProfile.initialCurrency = sequenceClass.initialcurrency;
        currentProfile.visualId = sequenceClass.visualid;
        this.clientProfile = currentProfile;
    }
}

[System.Serializable]
public enum ClientPos
{
    NONE,
    INIT,
    COUNTER,
    FINISH
}

[System.Serializable]
public class SequenceClass
{
    public int id;
    public int initialcurrency;
    public int visualid;
}
