﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ItemInfo : MonoBehaviour
{
    public int slotID;
    public bool isFill;

    public Button itemButton;
    public Image profilePicture;
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI dollarText;
    public Image panelImage;
    public GameObject glowImageObject;


    public GameObject dataSetObject;
    public GameObject emptySlotGameobject;
    public Sprite dataSetSprite;
    public Sprite selectedSprite;


    public Image profileHighlight;
    public Sprite profileSelect;
    public Sprite profileNotSelect;


    public Color32 initColor;
    public Color32 selectedColor;

    public PPLoader ppLoader;

    public ClientProfile clientProfile;

    UnityAction<ClientProfile> onButtonClick;

    private void Start()
    {
        itemButton.onClick.AddListener(delegate
        {
            onButtonClick(clientProfile);
            SelectedState();
        });
    }

    public void DeactivateItem()
    {
        gameObject.SetActive(false);
    }
   

    public void SetClientInfo(ClientProfile clientProfile, UnityEngine.Events.UnityAction<ClientProfile> onButtonClick)
    {
        this.onButtonClick = onButtonClick;
        DataSetState();
        DeactivateEmptySlot();
        this.clientProfile = clientProfile;
        nameText.text = clientProfile.clientInformation.title;
        dollarText.text = "$"+clientProfile.initialCurrency.ToString();
        isFill = true;
        itemButton.interactable = true;

       
        profilePicture.sprite = ppLoader.GetVisualProfilePicture(clientProfile.visualId);
    }
    public void SelectedState()
    {
        //panelImage.sprite = selectedSprite;
        glowImageObject.SetActive(true);
        nameText.color = selectedColor;
        profileHighlight.sprite = profileSelect;
    }
    public void DataSetState()
    {
        dataSetObject.SetActive(true);
        nameText.color = initColor;
        panelImage.sprite = dataSetSprite;
        glowImageObject.SetActive(false);
        profileHighlight.sprite = profileNotSelect; 
    }
    public void EmptyState()
    {
        dataSetObject.SetActive(false);
        emptySlotGameobject.SetActive(true);
        itemButton.interactable = false;
        glowImageObject.SetActive(false);
    }
    public void DeactivateEmptySlot()
    {
        emptySlotGameobject.SetActive(false);
    }

    public void Reset()
    {
        isFill = false;
        EmptyState();
    }
}
