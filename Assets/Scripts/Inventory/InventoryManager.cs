﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager Instance;

    public GameObject inventoryObject;

    public List<ItemInfo> itemList;

    public UIBiodata biodata;
    public TextMeshProUGUI offerValueText;


    public Button backButton;
    public Button matchButton;

    public CharLoader charLoader;


    UnityAction<ClientProfile> OnButtonClick;

    public ClientProfile currentSelectedProfileInventory;
    public List<ClientProfile> clientProfiles;


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        backButton.onClick.AddListener(delegate
        {
            if (UIManager.Instance.isFromInventoryButton)
            {
                DeactivateInventory();
                UIManager.Instance.ActivateInventoryButton();
                UIManager.Instance.ActivateLevelText();
                if (EntryHallManager.Instance.isReachedToCounter)
                {
                    StatsUI.Instance.ActivateStatsPanel();
                    UIManager.Instance.ActivateDecisionPanel();
                    StatsUI.Instance.SetBiodata(Gameplay.Instance.currentLevelData.clientProfile);
                }
                UIManager.Instance.isFromInventoryButton = false;
                EntryHallManager.Instance.isBackToBaseLoop = true;
                GameStateManager.RequestSwitch(GameState.BaseLoop);
            }
            else
            {
                DeactivateInventory();
                EntryHallManager.Instance.isBackToBaseLoop = true;
                UIManager.Instance.ActivateLevelText();
                UIManager.Instance.ActivateInventoryButton();
                GameStateManager.RequestSwitch(GameState.BaseLoop);
            }
            Gameplay.Instance.UpdateCurrentLevelData();
        });

        matchButton.onClick.AddListener(delegate
        {
            DataManager.Instance.SetInvenotrySelectedProfile(currentSelectedProfileInventory);
            DataManager.Instance.SetInventoryProfileList(clientProfiles);
            GameStateManager.RequestSwitch(GameState.Matchmaking);

            Gameplay.Instance.GotoMatchmakingScene();
        });
    }


    public void SetAllSlotWithSavedList(List<Slot> savedSlotList)
    {
        clientProfiles = new List<ClientProfile>();

        OnButtonClick += ClickButton;
        for (int i = 0; i < itemList.Count; i++)
        {
            //itemList[i].DeactivateItem();
        }
        for (int i = 0; i < savedSlotList.Count; i++)
        {
            Slot slot = savedSlotList[i];
            ClientProfile clientProfile = GameDataReader.Instance.GetClientProfileWithID(slot.savedProfileID);

            clientProfiles.Add(clientProfile);

            itemList[i].SetClientInfo(clientProfile,OnButtonClick);
        }
    }

    private void ClickButton(ClientProfile clientProfile)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].isFill)
            {
                itemList[i].DataSetState();
            }
        }
        SetBiodata(clientProfile);
        currentSelectedProfileInventory = clientProfile;

        int visualID = clientProfile.visualId;
        charLoader.ActivateVisualChar(visualID);
    }

    public void SetCurrentProfileInSlot(ClientProfile clientProfile)
    {
        List<Slot> slots = DataManager.Instance.Inventory.GetSavedSlotList();
        if (slots.Count > 0)
        {
            SetAllSlotWithSavedList(slots);
            itemList[slots.Count + 1].SetClientInfo(clientProfile, OnButtonClick);
        }
        else
        {
            itemList[0].SetClientInfo(clientProfile, OnButtonClick);
        }

        
    }

    public void HighlightCurrentSlot(ClientProfile clientProfile)
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            if (itemList[i].isFill)
            {
                if(clientProfile.clientID == itemList[i].clientProfile.clientID)
                {
                    ClickButton(clientProfile);   
                    itemList[i].SelectedState();
                }
            }
        }

        this.currentSelectedProfileInventory = clientProfile;
    }

    public void ActivateInventory()
    {
        inventoryObject.SetActive(true);
    }
    public void DeactivateInventory()
    {
        inventoryObject.SetActive(false);
    }
    public void ResetInventoryData()
    {
        for (int i = 0; i < itemList.Count; i++)
        {
            itemList[i].Reset();
        }
    }

    public void SetBiodata(ClientProfile clientProfile)
    {
        //List<string> stringList = new List<string>();

        //stringList = ClientHelper.GetOptionalDisplayInfoFields(clientProfile);

        biodata.SetBiodata(clientProfile, true);
       
        offerValueText.text = "$"+clientProfile.initialCurrency.ToString();
    }
}
