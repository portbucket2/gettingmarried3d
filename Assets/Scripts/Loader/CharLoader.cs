﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharLoader : MonoBehaviour
{
    public List<VisualIDStruct> visualIdList;

    public void ActivateVisualChar(int id)
    {
        foreach (var visualid in visualIdList)
        {
            visualid.visualObject.SetActive(false);
        }
        foreach (var visualID in visualIdList)
        {
            if(visualID.visualid == id)
            {
                visualID.visualObject.SetActive(true);
            }
        }
    }
}

[System.Serializable]
public struct VisualIDStruct
{
    public int visualid;
    public GameObject visualObject;
}