﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIBiodata : MonoBehaviour
{
    public static UIBiodata Instance;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public List<TextMeshProUGUI> biodataTextList;

    public TextMeshProUGUI lookingForTitle;


    public void SetBiodata(ClientProfile clientProfile, bool isLookingFor = false, ClientInfo expectedRequirements = null)
    {
        List<string> stringList = new List<string>();

        stringList = ClientHelper.GetOptionalDisplayInfoFields(clientProfile, expectedRequirements);

        for (int i = 0; i < biodataTextList.Count; i++)
        {
            biodataTextList[i].gameObject.SetActive(false);
        }

        for (int i = 0; i < stringList.Count; i++)
        {
            biodataTextList[i].gameObject.SetActive(true);
            biodataTextList[i].text = stringList[i];
        }

        if (isLookingFor)
        {
            lookingForTitle.gameObject.SetActive(true);
            lookingForTitle.text = clientProfile.clientRequirement.title;
        }
    }
}
