﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PPLoader : MonoBehaviour
{
    public List<PPStruct> ppList;

    public Sprite GetVisualProfilePicture(int id)
    {
        Sprite sprite = null;
        foreach (var visualID in ppList)
        {
            if (visualID.visualid == id)
            {
                sprite = visualID.sprite;
            }
        }
        return sprite;
    }
}

[System.Serializable]
public struct PPStruct
{
    public int visualid;
    public Sprite sprite;
}
