﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using FRIA;

public class MatchamakingManager : MonoBehaviour
{
    public static HardData<int> matchNumber;

    

    public static MatchamakingManager Instance;

    public int currentIndex;

    public UIBiodata selectedBiodata;

    public UIBiodata changingBiodata;

    public Button nextButton;
    public Button prevButton;

    public Button matchButton;
    public Button rejectButton;
    public Button saveLaterButton;

    public TextMeshProUGUI percentageText;
    public Animator percentageAnimator;

    public Animator holderAnimator;
    public Animator buttonpanelAnimator;
    

    public GameObject successPanel;
    public GameObject failedPanel;

    public Button successTap;
    public Button failedTap;

    public GameObject warningObject;

    [Header("Animation part")]
    public CharLoader fixedCharLoader;
    public CharLoader variableCharLoader;

    public Animator fixedCharAnimator;
    public Animator variableCharAnimator;

    public Animator cameraAnimator;
    public ParticleSystem loveParticle;

    public TextMeshProUGUI incomeText;

    public GameObject cashUpObject;
    public TextMeshProUGUI cashupText;

    public ClientProfile currentSelectProfile;
    public List<ClientProfile> clientList;


    private int TALKING_2 = Animator.StringToHash("talking2");
    private int SLIDE = Animator.StringToHash("slide");
    private int SUCCESS_FEMALE = Animator.StringToHash("successfemale");
    private int SUCCESS_MALE = Animator.StringToHash("successmale");
    private int FAILED_ACCEPT = Animator.StringToHash("failedaccept");
    private int FAILED_REJECT = Animator.StringToHash("failedreject");
    private int FAILED = Animator.StringToHash("failed");


    public int incomeValue;

    private void Awake()
    {
        ParkEnvLoad.Instance.parkEnv.SetActive(true);

        if(Instance == null)
        {
            Instance = this;

        }
        if(matchNumber == null)
        {
            matchNumber = new HardData<int>("MATCH_NUMBER",0);
        }

        clientList = new List<ClientProfile>();
        currentIndex = 0;

        clientList = DataManager.Instance.GetAllSavedClientProfiles(); //DataManager.Instance.inventoryClientList;
    }

    private void Start()
    {
        currentSelectProfile = DataManager.Instance.inventorySelectedProfile;
        selectedBiodata.SetBiodata(currentSelectProfile, true);
        fixedCharLoader.ActivateVisualChar(currentSelectProfile.visualId);

        int val = UnityEngine.Random.Range(0, 100);
        Debug.Log("VAL   ---- "+val);
        if (val > 50)
        {
            fixedCharAnimator.SetTrigger(TALKING_2);
        }


       
            CreateTempList();
            InitBiodataset();
        if (clientList.Count > 0)
        {
            UpdateUI(clientList[currentIndex]);
        }

        ButtonInteraction();

        if (DataManager.Instance.Inventory.GetSlotCount() == 6)
        {
            saveLaterButton.interactable = false;
        }
        else
        {
            saveLaterButton.interactable = true;
        }
    }

    private void CreateTempList()
    {
       
            for (int i = 0; i < clientList.Count; i++)
            {
                if (currentSelectProfile.clientID == clientList[i].clientID)
                {
                    clientList.RemoveAt(i);
                }
            }
        
    }

    private void ButtonInteraction()
    {
        nextButton.onClick.AddListener(delegate
        {
            currentIndex++;
            if(currentIndex == clientList.Count)
            {
                currentIndex -= 1;
                Debug.LogWarning("LIMIT EXCEED");
                nextButton.interactable = false;
            }


            prevButton.interactable = true;
            ButtonBiodataSet(currentIndex);
        });
        prevButton.onClick.AddListener(delegate
        {
            currentIndex--;
            if(currentIndex < 0)
            {
                currentIndex = 0;
                Debug.LogWarning("LIMIT LESS THAN ZEROOOOO");
                prevButton.interactable = false;
            }
            nextButton.interactable = true;
            ButtonBiodataSet(currentIndex);
        });

        matchButton.onClick.AddListener(delegate
        {
            StartCoroutine(MatchRoutine());
        });
        rejectButton.onClick.AddListener(delegate
        {
            GameStateManager.RequestSwitch(GameState.BaseLoop);
            AnalyticsAssitantGM.ReportReject(LevelManager.Instance.gameCurrentLevel);

            DataManager.Instance.isBackFromMatchmaking = true;
            Gameplay.Instance.GotoGameplayScene();
        });
        saveLaterButton.onClick.AddListener(delegate
        {
            GameStateManager.RequestSwitch(GameState.BaseLoop);
            AnalyticsAssitantGM.ReportSaveForLater(LevelManager.Instance.gameCurrentLevel);

            Slot slot = new Slot();
            slot.savedProfileID = Gameplay.Instance.currentLevelData.clientProfile.clientID;
            DataManager.Instance.Inventory.AddSlotToSave(slot);

            DataManager.Instance.SaveData();

            DataManager.Instance.isBackFromMatchmaking = true;
            Gameplay.Instance.GotoGameplayScene();
        });
        successTap.onClick.AddListener(delegate
        {
            GameStateManager.RequestSwitch(GameState.BaseLoop);
            DataManager.Instance.isBackFromMatchmaking = true;
            Gameplay.Instance.GotoGameplayScene();
        });
        failedTap.onClick.AddListener(delegate
        {
            GameStateManager.RequestSwitch(GameState.BaseLoop);
            DataManager.Instance.isBackFromMatchmaking = true;
            Gameplay.Instance.GotoGameplayScene();
        });
    }

    public enum DecisionFinal
    {
        failed = 0,
        firstSatisfied = 1,
        secondSatisfied = 2,
        success = 3,
        perfect = 4
    }

    IEnumerator MatchRoutine()
    {
        Currency.Transaction(CurrencyType.CASH, -200);

        GameStateManager.RequestSwitch(GameState.Result);
        yield return null;
       // backAnimator.enabled = true;
        buttonpanelAnimator.enabled = true;
        percentageAnimator.enabled = true;
        yield return new WaitForSeconds(0.75f);
        holderAnimator.enabled = true;

        ClientProfile first = currentSelectProfile;
        ClientProfile second = clientList[currentIndex];

        DecisionFinal decisionFinal = DecisionFinal.failed;


        int firstScore = ClientHelper.GetScore(first.clientRequirement, second.clientInformation);
        Debug.LogWarning("MAtchmking FIRST SCORE  :" + firstScore);

        int secondScore = ClientHelper.GetScore(second.clientRequirement, first.clientInformation);
        Debug.LogWarning("MAtchmking SECOND SCORE  :" + secondScore);

        int totalScore = firstScore + secondScore;

        if(firstScore>0 && secondScore > 0)
        {
            if(firstScore+secondScore == 200)
            {
                decisionFinal = DecisionFinal.perfect; // male happy, female happy
            }
            else
            {
                decisionFinal = DecisionFinal.success; // male happy, female happy
            }
        }
        else if (firstScore > 0)
        {
            decisionFinal = DecisionFinal.firstSatisfied; //first guy cry, second guy reject
        }
        else if (secondScore > 0)
        {
            decisionFinal = DecisionFinal.secondSatisfied; // first guy reject, second guy cry
        }
        else
        {
            decisionFinal = DecisionFinal.failed; // both angry
        }

        cameraAnimator.enabled = true;

        yield return new WaitForSeconds(0.5f);
        fixedCharLoader.gameObject.GetComponent<Animator>().enabled = true;
        variableCharLoader.gameObject.GetComponent<Animator>().enabled = true;
        fixedCharAnimator.SetTrigger(SLIDE);
        variableCharAnimator.SetTrigger(SLIDE);

        yield return new WaitForSeconds(1f);

        matchNumber.value++;

        switch (decisionFinal)
        {
            default:
                AnalyticsAssitantGM.ReportMatchFailed(matchNumber.value);
                break;

            case DecisionFinal.success:
            case DecisionFinal.perfect:
                AnalyticsAssitantGM.ReportMatchSuccess(matchNumber.value);
                break;
        }


        switch (decisionFinal)
        {
            case DecisionFinal.failed:
                PlayFailed();
                PlayVariableFailed();
                break;

            case DecisionFinal.firstSatisfied:
                PlayFailedAccpet();
                PlayVariableFailedReject();
                break;

            case DecisionFinal.perfect:
                loveParticle.Play();
                if (currentSelectProfile.clientInformation.gender == Gender.Male)
                {
                    PlayFixedSuccessMale();
                }
                else
                {
                    PlayFixedSuccessFemale();
                }

                if(second.clientInformation.gender == Gender.Male)
                {
                    PlayVariableSuccessMale();
                }
                else
                {
                    PlayVariableSuccessFemale();
                }

                break;


            case DecisionFinal.secondSatisfied:
                PlayFailedReject();
                PlayVariableFailedAccpet();
                break;


            case DecisionFinal.success:
                loveParticle.Play();
                if (currentSelectProfile.clientInformation.gender == Gender.Male)
                {
                    PlayFixedSuccessMale();
                }
                else
                {
                    PlayFixedSuccessFemale();
                }

                if (second.clientInformation.gender == Gender.Male)
                {
                    PlayVariableSuccessMale();
                }
                else
                {
                    PlayVariableSuccessFemale();
                }

                break;

            default:
                break;
        }


        yield return new WaitForSeconds(3f);

        //remove ids from list
        DataManager.Instance.Inventory.DeleteSavedSlot(currentSelectProfile.clientID);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        DataManager.Instance.Inventory.DeleteSavedSlot(clientList[currentIndex].clientID);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        DataManager.Instance.SaveData();
        yield return new WaitForEndOfFrame();

        yield return new WaitForSeconds(2f);

        if (decisionFinal == DecisionFinal.perfect || decisionFinal == DecisionFinal.success)
        {
            successPanel.SetActive(true);
            int finalCash = Mathf.CeilToInt(incomeValue * (totalScore / 200f));
            Currency.Transaction(CurrencyType.CASH, +finalCash);
            cashupText.text = "+"+finalCash.ToString();
            cashUpObject.SetActive(true);
        }
        else
        {
            failedPanel.SetActive(true);
        }

        yield return new WaitForSeconds(1f);
    }

    public void UpdateUI(ClientProfile tempChanging)
    {
        changingBiodata.SetBiodata(tempChanging, true, currentSelectProfile.clientRequirement);
        selectedBiodata.SetBiodata(currentSelectProfile, true, tempChanging.clientRequirement);
        variableCharLoader.ActivateVisualChar(tempChanging.visualId);

        incomeValue = tempChanging.initialCurrency + currentSelectProfile.initialCurrency;
        incomeText.text = "$" + incomeValue.ToString();

        int A = ClientHelper.GetScore(currentSelectProfile.clientRequirement, tempChanging.clientInformation);
        int B = ClientHelper.GetScore(tempChanging.clientRequirement, currentSelectProfile.clientInformation);

        percentageText.text = ((A + B) / 2).ToString() + "%" + " Match";
    }

    public void ButtonBiodataSet(int index)
    {
        ClientProfile tempChanging = clientList[index];
        UpdateUI(tempChanging);

        int A = ClientHelper.GetScore(currentSelectProfile.clientRequirement, tempChanging.clientInformation);
        int B = ClientHelper.GetScore(tempChanging.clientRequirement, currentSelectProfile.clientInformation);

        percentageText.text = ((A + B) / 2).ToString() + "%" +" Match";

        //if (currentSelectProfile.clientID == clientList[index].clientID)
        //{
        //    ClientProfile tempChanging = clientList[index+1];
        //    changingBiodata.SetBiodata(tempChanging, true);
        //    variableCharLoader.ActivateVisualChar(tempChanging.visualId);
        //}
        //else
        //{
        //    ClientProfile tempChanging = clientList[index];
        //    changingBiodata.SetBiodata(tempChanging, true);
        //    variableCharLoader.ActivateVisualChar(tempChanging.visualId);
        //}
    }

    public void InitBiodataset()
    {
        
            if (clientList.Count > 0)
            {
                percentageAnimator.gameObject.SetActive(true);

                if (DataManager.Instance.isScrollMatch)
                {
                    for (int i = 0; i < clientList.Count; i++)
                    {
                        if (DataManager.Instance.scrollSelectProfile.clientID == clientList[i].clientID)
                        {
                            ClientProfile tempChanging = clientList[i];
                            changingBiodata.SetBiodata(tempChanging, true);
                            currentIndex = i;
                            variableCharLoader.ActivateVisualChar(tempChanging.visualId);
                        }
                    }
                }
                else
                {
                    if (currentSelectProfile.clientID == clientList[0].clientID)
                    {
                        ClientProfile tempChanging = clientList[1];
                        changingBiodata.SetBiodata(tempChanging, true);
                        currentIndex = 1;
                        variableCharLoader.ActivateVisualChar(tempChanging.visualId);
                    }
                    else
                    {
                        ClientProfile tempChanging = clientList[0];
                        changingBiodata.SetBiodata(tempChanging, true);
                        currentIndex = 0;
                        variableCharLoader.ActivateVisualChar(tempChanging.visualId);
                    }
                    changingBiodata.gameObject.SetActive(true);
                    warningObject.SetActive(false);
                    matchButton.interactable = true;
                    variableCharLoader.gameObject.SetActive(true);
                }
            }
            else
            {
                changingBiodata.gameObject.SetActive(false);
                warningObject.SetActive(true);
                matchButton.interactable = false;
                variableCharLoader.gameObject.SetActive(false);
                percentageAnimator.gameObject.SetActive(false);
            }
        
    }
    public void PlayFixedSlide()
    {
        fixedCharAnimator.SetTrigger(SLIDE);
    }
    public void PlayVariableSlide()
    {
        variableCharAnimator.SetTrigger(SLIDE);
    }

    public void PlayFixedSuccessFemale()
    {
        fixedCharAnimator.SetTrigger(SUCCESS_FEMALE);
    }
    public void PlayFixedSuccessMale()
    {
        fixedCharAnimator.SetTrigger(SUCCESS_MALE);
    }
    public void PlayVariableSuccessFemale()
    {
        variableCharAnimator.SetTrigger(SUCCESS_FEMALE);
    }
    public void PlayVariableSuccessMale()
    {
        variableCharAnimator.SetTrigger(SUCCESS_MALE);
    }

    public void PlayFailedAccpet()
    {
        fixedCharAnimator.SetTrigger(FAILED_ACCEPT);
    }
    public void PlayFailedReject()
    {
        fixedCharAnimator.SetTrigger(FAILED_REJECT);
    }
    public void PlayFailed()
    {
        fixedCharAnimator.SetTrigger(FAILED);
    }

    public void PlayVariableFailedAccpet()
    {
        variableCharAnimator.SetTrigger(FAILED_ACCEPT);
    }
    public void PlayVariableFailedReject()
    {
        variableCharAnimator.SetTrigger(FAILED_REJECT);
    }
    public void PlayVariableFailed()
    {
        variableCharAnimator.SetTrigger(FAILED);
    }


}
