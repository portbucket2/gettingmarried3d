﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;
    Inventory inventory;

    private readonly string INVENTORY_DATA = "/married.gm";


    public ClientProfile inventorySelectedProfile;
    public List<ClientProfile> inventoryClientList;
    internal bool isBackFromMatchmaking;
    public bool isInventoryPopulate;
    public bool isScrollMatch;
    public ClientProfile scrollSelectProfile;

    private int invenPopulate;
    private string INVENTORY_POPULATE = "Inventory_Populate";

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            DestroyImmediate(this.gameObject);
        }

        inventory = new Inventory();

        invenPopulate = PlayerPrefs.GetInt(INVENTORY_POPULATE, 0);
        
    }
    private void Start()
    {
        LoadData();
        if (invenPopulate == 0)
        {
            PlayerPrefs.SetInt(INVENTORY_POPULATE, 1);
            StartCoroutine(InventoryPolulate());
            Debug.LogWarning("Inventory populate for first Time");
        }
        DontDestroyOnLoad(this.gameObject);
    }

    private IEnumerator InventoryPolulate()
    {
        yield return new WaitForSeconds(0.1f);
        if (isInventoryPopulate)
        {
           
            for (int i = 1; i <= 3; i++)
            {
                //SequenceClass sequenceClass = GameDataReader.Instance.sequenceClassList[i-1];
                //Debug.Log(sequenceClass.id);

                //ClientProfile currentProfile = new ClientProfile();
                //currentProfile = GameDataReader.Instance.GetClientProfile(sequenceClass.id);
                //currentProfile.initialCurrency = sequenceClass.initialcurrency;
                //currentProfile.visualId = sequenceClass.visualid;

                ClientProfile clientProfile = GameDataReader.Instance.GetClientProfileWithID(i);

                Slot slot = new Slot();
               
                slot.savedProfileID = clientProfile.clientID;

                inventory.AddSlotToSave(slot);
                SaveData();
                yield return new WaitForEndOfFrame();
            }
            LoadData();
        }
    }


    #region Public Functions

    public Inventory Inventory
    {
        get
        {
            return inventory;
        }
    }

    public void SaveData()
    {
        BinaryFormatter formatter = new BinaryFormatter();

        string path = Application.persistentDataPath + INVENTORY_DATA;
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, inventory);

        stream.Close();
    }

    public void LoadData()
    {
        string path = Application.persistentDataPath + INVENTORY_DATA;
        Debug.LogWarning(path);
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            inventory = formatter.Deserialize(stream) as Inventory;
            stream.Close();
        }
        else
        {
            Debug.LogError("Save File Not found in " + path);
            return;
        }

        for (int i = 0; i < inventory.GetSavedSlotList().Count; i++)
        {
//            Debug.LogError(inventory.GetSavedSlotList()[i].savedProfileID + " saved profile id");
        }
    }

    public List<ClientProfile> GetAllSavedClientProfiles()
    {
        List<ClientProfile> clientProfiles = new List<ClientProfile>();
        List<Slot> savedSlotList = new List<Slot>();
        savedSlotList = inventory.GetSavedSlotList();
        if (savedSlotList.Count > 0)
        {
            for (int i = 0; i < savedSlotList.Count; i++)
            {
                Slot slot = savedSlotList[i];
                ClientProfile clientProfile = GameDataReader.Instance.GetClientProfileWithID(slot.savedProfileID);

                clientProfiles.Add(clientProfile);
            }
        }

        if (clientProfiles.Count == 0) return new List<ClientProfile>();
        else return clientProfiles;
    }

    public void SetInvenotrySelectedProfile(ClientProfile clientProfile)
    {
        inventorySelectedProfile = new ClientProfile();
        inventorySelectedProfile = clientProfile;

        Debug.LogWarning(inventorySelectedProfile.clientInformation.title);
    }
    public void SetInventoryProfileList(List<ClientProfile> clientProfiles)
    {
        inventoryClientList = new List<ClientProfile>();
        inventoryClientList = clientProfiles;
    }

    #endregion


}
