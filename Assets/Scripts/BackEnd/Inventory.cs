﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory
{
    [SerializeField]
    private int currency;
    [SerializeField]
    private int turn;

    [SerializeField]
    private List<Slot> savedSlotList;

    public Inventory()
    {
        savedSlotList = new List<Slot>();
    }



    #region Public Functions

    public void AddSlotToSave(Slot slot)
    {
        if (savedSlotList.Count == 0)
        {
            savedSlotList.Add(slot);
        }
        else
        {
            for (int i = 0; i < savedSlotList.Count; i++)
            {
                if(savedSlotList[i].savedProfileID == slot.savedProfileID)
                {
                    return;
                }
            }
            for (int i = 0; i < savedSlotList.Count; i++)
            {
                if (savedSlotList[i].savedProfileID != slot.savedProfileID)
                {
                    savedSlotList.Add(slot);
                    break;
                }
            }
        }
    }

    public void DeleteSavedSlot(int id)
    {
        for (int i = 0; i < savedSlotList.Count; i++)
        {
            if (savedSlotList[i].savedProfileID == id)
            {
                Debug.LogError("Remove from " + i + " Prev count " + savedSlotList.Count);
                savedSlotList.RemoveAt(i);
                Debug.LogError("Remove from " + i + " New count " + savedSlotList.Count);
            }
        }
    }
    public List<Slot> GetSavedSlotList()
    {
        if (savedSlotList.Count ==0) return new List<Slot>();
        else return savedSlotList;
    }
    public Slot GetSlot(int t_SlotID)
    {
        Slot slot = new Slot();
        foreach (var item in savedSlotList)
        {
            if(item.slotID == t_SlotID)
            {
                slot = item;
            }
        }
        return slot;
    }
    public int GetSlotCount()
    {
        if (savedSlotList.Count < 1) return 0;
        else return savedSlotList.Count;
    }
    public bool IsSlotEmpty(int slotID)
    {
        bool isEmpty = false;
        foreach (var item in savedSlotList)
        {
            if (item.slotID == slotID)
            {
                isEmpty = item.isSlotEmpty;
            }
        }
        return isEmpty;
    }
    public void SetCurrency(int t_Value)
    {
        this.currency = t_Value;
    }
    public void SetCurrentTurn(int t_Value)
    {
        turn = t_Value;
    }
    public void IncreaseCurrentTurn()
    {
        turn++;
    }
    public int Currency
    {
        get
        {
            return currency;
        }
    }
    public int Turn
    {
        get
        {
            return turn;
        }
    }
    #endregion
}
