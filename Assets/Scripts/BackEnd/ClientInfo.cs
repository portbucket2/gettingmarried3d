﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class ClientInfo
{
    public int id;
    public string title;
    public Gender gender;
    public Profession profession;
    public int age;
    public int height;
    public int fitness;
    public int humor;
    public int income;
    public int fame;
    public int intellect;

}
[System.Serializable]
public enum Profession
{
    NONE = 0,
    Athlete = 1,
    Corporate = 2,
    Scientist = 3,
    Influencer = 4,
    Musician = 5,
    Agent = 6,
}