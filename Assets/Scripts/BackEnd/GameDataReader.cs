﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;
using System;

public class GameDataReader : MonoBehaviour
{
    public static GameDataReader Instance;

    public TextAsset getMarriedClientInfo;
    public TextAsset getMarriedClientRequireInfo;
    public TextAsset getSequenceInfo;

    public List<ClientInfo> clientInfoList = new List<ClientInfo>();
    public List<ClientInfo> clientRequireList = new List<ClientInfo>();

    public List<SequenceClass> sequenceClassList = new List<SequenceClass>();

    public List<ClientProfile> clientProfileList;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }

        CSVReader.Parse_toClassWithBasicConstructor(clientInfoList, getMarriedClientInfo);
        CSVReader.Parse_toClassWithBasicConstructor(clientRequireList, getMarriedClientRequireInfo);
        CSVReader.Parse_toClassWithBasicConstructor(sequenceClassList, getSequenceInfo);

        clientProfileList = new List<ClientProfile>();
    }

    private void Start()
    {
        GenerateClientProfile(clientInfoList, clientRequireList);

    }
    private void GenerateClientProfile(List<ClientInfo> clientInfoList,List<ClientInfo> requirementList)
    {
        for (int i = 0; i < clientInfoList.Count; i++)
        {
            ClientProfile t_ClientProfile = new ClientProfile();

            t_ClientProfile.name = clientInfoList[i].title;
            t_ClientProfile.clientID = clientInfoList[i].id;
            t_ClientProfile.clientInformation = clientInfoList[i];
            t_ClientProfile.clientRequirement = requirementList[i];
            t_ClientProfile.initialCurrency = sequenceClassList[i].initialcurrency;
            t_ClientProfile.visualId = sequenceClassList[i].visualid;


            clientProfileList.Add(t_ClientProfile);
        }
    }

    #region Public Functions

    public List<ClientProfile> GetAllClientProfile()
    {
        if (clientProfileList.Count < 1)
        {
            return null;
        }
        else
        {
            return clientProfileList;
        }
    }
    public ClientProfile GetClientProfile(int levelNumber)
    {
        ClientProfile clientProfile = new ClientProfile();
        
        foreach (var item in clientProfileList)
        {
            if(item.clientID == levelNumber)
            {
                clientProfile = item;
            }
        }

        return clientProfile;
    }
    public ClientProfile GetClientProfileWithID(int clientID)
    {
        ClientProfile clientProfile = new ClientProfile();
        foreach (var item in clientProfileList)
        {
            if (item.clientID == clientID)
            {
                clientProfile = item;
            }
        }
        

        return clientProfile;
    }

    #endregion

}
