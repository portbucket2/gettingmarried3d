﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System;

[System.Serializable]
public static class ClientHelper
{ 
    public static bool ShowScoreLog = false;

    private static List<float> fontSizeList = new List<float>();

    public static List<string> GetOptionalDisplayInfoFields(ClientProfile profile,  ClientInfo requirementContext=null)
    {
        fontSizeList = new List<float>();
        fontSizeList.Add(0f);
        fontSizeList.Add(0f);

        ClientInfo cinfo = profile.clientInformation;

        Dictionary<string,string> rqList = GetRichHeaders(requirementContext,profile.clientInformation);
        List<string> displayList = new List<string>();
        //displayList.Clear();

        Type t_class = typeof(ClientInfo);
        TypeInfo typeInfo = t_class.GetTypeInfo();
        FieldInfo[] fis = typeInfo.GetFields();

        string title = profile.clientInformation.title;

        string gender = profile.clientInformation.gender.ToString();
        string profession = profile.clientInformation.profession.ToString();
        gender = rqList["gender"] + gender + highTail;
        profession = rqList["profession"] + profession + highTail;
        string subtitle =string.Format("{0}-{1}",gender, profession);

        displayList.Add(title);
        displayList.Add(subtitle);
        foreach (FieldInfo finfo in fis)
        {
            if (finfo.Name == "id") continue;
            Type t = finfo.FieldType;
            object infoVal = finfo.GetValue(cinfo);
            if (t == typeof(int))
            {
                int iVal = (int)infoVal;
                if (iVal != (int) GetDefault(t))
                {
                    //Debug.Log(finfo.Name);
                    displayList.Add(GetDisplayStringNewFormat(finfo, (int)infoVal,rqList));
                }
            }
            else if (t == typeof(float))
            {
                float fVal = (float)infoVal;
                if (fVal != (float) GetDefault(t))
                {
                    //Debug.Log(finfo.Name);
                    displayList.Add(GetDisplayStringNewFormat(finfo, (float)infoVal,rqList));
                }
            }
        }

        return displayList;

    }

    public static List<string> GetOptionalDisplayInfoFieldsEntryScene(ClientProfile profile, ClientInfo requirementContext = null)
    {
        fontSizeList = new List<float>();
        fontSizeList.Add(0f);
        fontSizeList.Add(0f);

        ClientInfo cinfo = profile.clientInformation;

        Dictionary<string, string> rqList = GetRichHeadersEntryScene(requirementContext, profile.clientInformation);
        List<string> displayList = new List<string>();
        //displayList.Clear();

        Type t_class = typeof(ClientInfo);
        TypeInfo typeInfo = t_class.GetTypeInfo();
        FieldInfo[] fis = typeInfo.GetFields();

        string title = profile.clientInformation.title;

        string gender = profile.clientInformation.gender.ToString();
        string profession = profile.clientInformation.profession.ToString();
        gender = rqList["gender"] + gender + highTail;
        profession = rqList["profession"] + profession + highTail;
        string subtitle = string.Format("{0}-{1}", gender, profession);

        displayList.Add(title);
        displayList.Add(subtitle);
        foreach (FieldInfo finfo in fis)
        {
            if (finfo.Name == "id") continue;
            Type t = finfo.FieldType;
            object infoVal = finfo.GetValue(cinfo);
            if (t == typeof(int))
            {
                int iVal = (int)infoVal;
                if (iVal != (int)GetDefault(t))
                {
                    //Debug.Log(finfo.Name);
                    displayList.Add(GetDisplayStringNewFormat(finfo, (int)infoVal, rqList));
                }
            }
            else if (t == typeof(float))
            {
                float fVal = (float)infoVal;
                if (fVal != (float)GetDefault(t))
                {
                    //Debug.Log(finfo.Name);
                    displayList.Add(GetDisplayStringNewFormat(finfo, (float)infoVal, rqList));
                }
            }
        }

        return displayList;

    }

    public static List<float> GetFontSizeList()
    {
        return fontSizeList;
    }


    const string colorDefault = "<color=#000000ff>";
    const string colorBad = "<color=#ff0000ff>";
    const string colorGood = "<color=#00aaffff>";
    public static Dictionary<string,string> GetRichHeaders(ClientInfo requirements, ClientInfo details)
    {
        Dictionary<string, string> richHeaders = new Dictionary<string, string>();
        Type t_class = typeof(ClientInfo);
        TypeInfo typeInfo = t_class.GetTypeInfo();
        FieldInfo[] fis = typeInfo.GetFields();

        foreach (FieldInfo finfo in fis)
        {
            if (finfo.Name == "id") continue;
            richHeaders.Add(finfo.Name, colorDefault);
            if (requirements == null) continue;
            Type t = finfo.FieldType;
            object requiredValue = finfo.GetValue(requirements);
            object actualValue = finfo.GetValue(details);
            if (t.IsEnum)
            {
                int reqNumber = (int)requiredValue;
                int actNumber = (int)actualValue;
                if (reqNumber != (int)GetDefault(t))
                {
                    if (reqNumber != actNumber)
                    {
                        richHeaders[finfo.Name] = colorBad;
                    }
                    else
                    {
                        richHeaders[finfo.Name] = colorGood;
                    }
                }
            }
            else if (t == typeof(int) || t == typeof(float))
            {
                float reqNumber;
                float actNumber;
                if (t == typeof(int))
                {
                    reqNumber = (int)requiredValue;
                    actNumber = (int)actualValue;
                }
                else
                {
                    reqNumber = (float)requiredValue;
                    actNumber = (float)actualValue;
                }

                if (reqNumber > 0)
                {

                    if (actNumber < reqNumber)
                    {
                        richHeaders[finfo.Name] = colorBad;
                    }
                    else
                    {
                        richHeaders[finfo.Name] = colorGood;
                    }
                }
                else if (reqNumber < 0)
                {
                    reqNumber = -reqNumber;
                    if (actNumber > reqNumber)
                    {
                        richHeaders[finfo.Name] = colorBad;
                    }
                    else
                    {
                        richHeaders[finfo.Name] = colorGood;
                    }
                }
                else//this is not a requirement
                {

                }
            }
            else//probably string name, title
            {

            }

        }

        return richHeaders;

    }

    const string colorWhite = ""; //"<color=#FFFFFF>"

    public static Dictionary<string, string> GetRichHeadersEntryScene(ClientInfo requirements, ClientInfo details)
    {
        Dictionary<string, string> richHeaders = new Dictionary<string, string>();
        Type t_class = typeof(ClientInfo);
        TypeInfo typeInfo = t_class.GetTypeInfo();
        FieldInfo[] fis = typeInfo.GetFields();

        foreach (FieldInfo finfo in fis)
        {
            if (finfo.Name == "id") continue;
            richHeaders.Add(finfo.Name, colorWhite);
            if (requirements == null) continue;
            Type t = finfo.FieldType;
            object requiredValue = finfo.GetValue(requirements);
            object actualValue = finfo.GetValue(details);
        }

        return richHeaders;

    }

    public static int GetScore(ClientInfo requirements, ClientInfo details)
    {
        Type t_class = typeof(ClientInfo);
        TypeInfo typeInfo = t_class.GetTypeInfo();
        FieldInfo[] fis = typeInfo.GetFields();

        float totalDeductibleFound = 0;
        float valueToDeduct = 0;
        foreach (FieldInfo finfo in fis)
        {
            if (finfo.Name == "id") continue;
            Type t = finfo.FieldType;
            object requiredValue = finfo.GetValue(requirements);
            object actualValue = finfo.GetValue(details);
            if (t.IsEnum)
            {
                int reqNumber = (int)requiredValue;
                int actNumber = (int)actualValue;
                if (reqNumber != (int) GetDefault(t))
                {
                    float fieldWeight = GetReductionWeight(finfo.Name);
                    totalDeductibleFound += fieldWeight;
                    if (reqNumber != actNumber)
                    {
                        valueToDeduct += fieldWeight;
                    }
                    if(ShowScoreLog) Debug.LogFormat("{0}-{1} deduct:{4},{2}/{3}",
                       finfo.Name,
                       (actNumber != reqNumber) ? fieldWeight : 0,
                       reqNumber, actNumber, (actNumber != reqNumber)
                       );
                }
            }
            else if (t == typeof(int) || t == typeof(float))
            {
                float reqNumber;
                float actNumber;
                if (t == typeof(int))
                {
                    reqNumber = (int)requiredValue;
                    actNumber = (int)actualValue;
                }
                else
                {
                    reqNumber = (float)requiredValue;
                    actNumber = (float)actualValue;
                }

                if (reqNumber > 0)
                {

                    float fieldWeight = GetReductionWeight(finfo.Name);
                    totalDeductibleFound += fieldWeight;

                    if (actNumber < reqNumber)
                    {
                        valueToDeduct += fieldWeight;
                    }
                    if (ShowScoreLog) Debug.LogFormat("{0}-{1},{2}/{3}",
                            finfo.Name,
                            (actNumber < reqNumber) ? fieldWeight : 0,
                            reqNumber, actNumber
                            );
                }
                else if (reqNumber < 0)
                {
                    float fieldWeight = GetReductionWeight(finfo.Name);

                    totalDeductibleFound += fieldWeight;

                    reqNumber = -reqNumber;
                    if (actNumber > reqNumber)
                    {
                        valueToDeduct += fieldWeight;
                    }
                    if (ShowScoreLog) Debug.LogFormat("{0}-{1},{2}/{3}", 
                        finfo.Name, 
                        (actNumber > reqNumber)? fieldWeight:0,
                        reqNumber, actNumber
                        );
                }
                else//this is not a requirement
                {
                    
                }
            }
            else//probably string name, title
            {

            }

        }

        float score = 100;
        if (totalDeductibleFound > 0)
        {
           score = 100 *(1 - ( valueToDeduct / totalDeductibleFound));
        }
        if (ShowScoreLog) Debug.LogFormat("{2},{0}/{1}", valueToDeduct, totalDeductibleFound, score);

        return Mathf.Clamp( Mathf.RoundToInt(score),0,100);

    }

    const string highTail = "</color>";
    public static string CaseFix(this string str)
    {
      
      if (str.Length == 0)
        return "";
      else if (str.Length == 1)
        return char.ToUpper(str[0]) + "";
      else
        return char.ToUpper(str[0]) + str.Substring(1);
    }

    static string GetDisplayStringNewFormat(FieldInfo finfo, float numberVal, Dictionary<string, string> rqList)
    {
        string fixName = finfo.Name.CaseFix();
        int intVal = Mathf.RoundToInt(numberVal);


        string head = rqList[finfo.Name];
        string tail = highTail;

        switch (finfo.Name)
        {
            default:
                Debug.LogErrorFormat("Unhandled requirement found! for type {0}", finfo.Name);
                return "";

            case "age":
                string age = GetAgeFormat(intVal);
              //  float ageFontSize = GetAgeFont(intVal);
               

                return String.Format("{0}{1}{2}", head, age, tail);

            case "height":
                // int feet = intVal / 12;
                // int inches = intVal - 12 * feet;
                string height = GetHeightFormat(intVal);
                //float heightSize = GetHeightFont(intVal);
                //fontSizeList.Add(heightSize);
            
                return String.Format("{0}{1}{2}",  head, height, tail);

            case "fitness":
                string fitness = GetFitnessFormat(intVal);
                //float fitSize = GetFitSize(intVal);
               // fontSizeList.Add(fitSize);

                return String.Format("{0}{1}{2}", head, fitness, tail);

            case "humor":
                string humer = GetHumerFormat(intVal);

                //float humSize = GetHumerSize(intVal);
                //fontSizeList.Add(humSize);

                return String.Format("{0}{1}{2}", head, humer, tail);
            case "intellect":

            case "fame":
                return String.Format("{0}: {1}{2}/5{3}", fixName, head, intVal, tail);

            case "income":
                string income = GetIncomeFormat(intVal);

                //float incomesize = GetIncomeSize(intVal);
               // fontSizeList.Add(incomesize);

                return String.Format("{0}{1}{2}", head, income, tail);

        }
    }

    private static float GetIncomeSize(int intVal)
    {
        float str = 0f;

        int tempVal = (intVal - 1000);
        str = tempVal * 0.00056f;

        return str;
    }

    private static float GetHumerSize(int intVal)
    {
        float str = 0f;

        str = intVal * 2f;

        return str;
    }

    private static float GetFitSize(int intVal)
    {
        float str = 0f;

        str = intVal * 2f;

        return str;
    }

    private static float GetHeightFont(int intVal)
    {
        float str = 0f;

        int tempVal = (intVal - 60);
        str = tempVal * 0.5f;

        return str;
    }

    private static float GetAgeFont(int intVal)
    {
        float str = 0f;

        int tempVal = (intVal - 18);
        str = tempVal * 0.239f;

        return str;
    }

    private static string GetIncomeFormat(int intVal)
    {
        string str = "";

        if (intVal >= 1000 && intVal <= 6000)
        {
            str = "Poor";
            fontSizeList.Add(45);
        }
        else if (intVal >= 6001 && intVal <= 12000)
        {
            str = "Stable";
            fontSizeList.Add(55);
        }
        else if (intVal >= 12001 && intVal <= 20000)
        {
            str = "Rich";
            fontSizeList.Add(65);
        }

        return str;
    }

    private static string GetHumerFormat(int intVal)
    {
        string str = "";

        if (intVal >= 1 && intVal <= 2)
        {
            str = "Serious";
            fontSizeList.Add(45);
        }
        else if (intVal >= 3 && intVal <= 4)
        {
            str = "Boring";
            fontSizeList.Add(55);
        }
        else if (intVal == 5)
        {
            str = "Funny";
            fontSizeList.Add(65);
        }

        return str;
    }

    private static string GetFitnessFormat(int intVal)
    {
        string str = "";

        if (intVal >= 1 && intVal <= 2)
        {
            str = "Unfit";
            fontSizeList.Add(45);
        }
        else if (intVal >= 3 && intVal <= 4)
        {
            str = "Fit";
            fontSizeList.Add(55);
        }
        else if (intVal == 5)
        {
            str = "Athletic";
            fontSizeList.Add(65);
        }

        return str;
    }

    private static string GetHeightFormat(int intVal)
    {
        string str = "";

        if (intVal >= 60 && intVal <= 66)
        {
            str = "Short";
            fontSizeList.Add(45);
        }
        else if (intVal >= 67 && intVal <= 72)
        {
            str = "Tall";
            fontSizeList.Add(55);
        }
        else if (intVal >= 73 && intVal <= 80)
        {
            str = "Giant";
            fontSizeList.Add(65);
        }

        return str;
    }

    private static string GetAgeFormat(int intVal)
    {
        string str = "";

        if(intVal >= 18 && intVal <= 32)
        {
            str = "Young";
            fontSizeList.Add(45);
        }
        else if(intVal >= 33 && intVal <= 46)
        {
            str = "Mature";
            fontSizeList.Add(55);
        }
        else if (intVal >= 47 && intVal <= 40)
        {
            str = "Old";
            fontSizeList.Add(65);
        }

        return str;
    }

    static string GetDisplayString(FieldInfo finfo, float numberVal, Dictionary<string, string> rqList)
    {
        string fixName = finfo.Name.CaseFix();
        int intVal = Mathf.RoundToInt(numberVal);


        string head = rqList[finfo.Name];
        string tail = highTail;

        switch (finfo.Name)
        {
            default:
                Debug.LogErrorFormat("Unhandled requirement found! for type {0}",finfo.Name);
                return "";
            case "age":
                return String.Format("{0}: {1}{2}{3}", fixName,head, intVal,tail);
            case "height":
                int feet = intVal / 12;
                int inches = intVal - 12 * feet;
                return String.Format("{0}: {1}{2}'{3}{4}", fixName,head,feet,inches,tail);
            case "fitness":
                //return String.Format("{0}: {1} lb", fixName, intVal);

            case "humor":
            case "intellect":
            case "fame":
                return String.Format("{0}: {1}{2}/5{3}", fixName, head,intVal,tail);

            case "income":
                return String.Format("{0}: {1}${2}{3}", fixName, head, intVal, tail);

        }
    }

    static float GetReductionWeight(string fieldName)
    {
        switch (fieldName)
        {
            default:
                Debug.LogError("Unhandled requirement found!");
                return 0;
            case "gender":
                return 75;
            case "profession":
                return 55;

            case "age":
                return 40;
            case "height":
                return 25;
            case "fitness":
                return 25;

            case "humor":
                return 35;
            case "income":
                return 35;
            case "intellect":
                return 35;
            case "fame":
                return 35;


        }
    }
    static object GetDefault(Type type)
    {
        if (type.IsValueType)
        {
            return Activator.CreateInstance(type);
        }
        return null;
    }
}