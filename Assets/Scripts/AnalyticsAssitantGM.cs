﻿using System.Collections;
using System.Collections.Generic;
using APSdk;
using UnityEngine;
using GameAnalyticsSDK;


public static class AnalyticsAssitantGM
{
    public static void ReportLevelStart(int levelNumber)
    {
        APFirebaseWrapper.Instance.LogFirebaseEvent("Level_Start", "level_number", levelNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.Start, "level_number",levelNumber.ToString());
    }
    private static void ReportLevelCompleted(int levelNumber)
    {
        APFirebaseWrapper.Instance.LogFirebaseEvent("Level_Complete", "level_number", levelNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.Complete, "level_number", levelNumber.ToString());
    }
    public static void ReportMatchSuccess(int matchNumber)
    {
        APFirebaseWrapper.Instance.LogFirebaseEvent("Match_Success", "match_number", matchNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.MatchSuccess, "match_number", matchNumber.ToString());
    }
    public static void ReportMatchFailed(int matchNumber)
    {
        APFirebaseWrapper.Instance.LogFirebaseEvent("Match_Failed", "match_number", matchNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.MatchFailed, "match_number", matchNumber.ToString());
    }
    public static void ReportFindMatch(int levelNumber)
    {
        ReportLevelCompleted(levelNumber);

        APFirebaseWrapper.Instance.LogFirebaseEvent("Client_Match", "level_number", levelNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.ClientMatch, "level_number", levelNumber.ToString());

        //GameAnalyticsSDK.GameAnalytics.NewDesignEvent("find_match");
    }
    public static void ReportSaveForLater(int levelNumber)
    {
        ReportLevelCompleted(levelNumber);

        APFirebaseWrapper.Instance.LogFirebaseEvent("Client_Save", "level_number", levelNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.ClientSave, "level_number", levelNumber.ToString());
    }
    public static void ReportReject(int levelNumber)
    {
        ReportLevelCompleted(levelNumber);

        APFirebaseWrapper.Instance.LogFirebaseEvent("Client_Reject", "level_number", levelNumber.ToString());
        APGameAnalyticsWrapper.Instance.ProgressionEvent(GAProgressionStatus.ClientReject, "level_number", levelNumber.ToString());
    }

}
