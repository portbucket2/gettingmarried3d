﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkEnvLoad : MonoBehaviour
{
    public static ParkEnvLoad Instance;

    public GameObject parkRef;

    public GameObject parkEnv;

    private void Start()
    {
        if(Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
            parkEnv = Instantiate(parkRef);
            parkEnv.transform.SetParent(this.transform);
            
        }
        else
        {
            
            DestroyImmediate(this.gameObject);
        }

        Instance.parkEnv.SetActive(false);
    }
}
