﻿using System;
using System.Collections;
using System.Collections.Generic;
using com.faithstudio.SDK;
using PathCreation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class EntryHallManager : MonoBehaviour
{
    public static EntryHallManager Instance;

    public ClientManager movementController;
    public LevelData currentLevelData;

    public Animator entryGateAnimator;
    public Animator exitGameAnimator;
    public Animator chairAnimator;

    public bool isReachedToCounter;


    

    private int ENTRY = Animator.StringToHash("entry");
    private int EXIT = Animator.StringToHash("exit");
    public bool isAccept;
    public bool isReject;

    public bool isBackToBaseLoop;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        movementController = GetComponent<ClientManager>();
    }

    public void StartEntryHallManager(LevelData currentLevelData)
    {
        AnalyticsAssitantGM.ReportLevelStart(LevelManager.Instance.gameCurrentLevel);

        this.currentLevelData = currentLevelData;

        UnityAction OnReachedToCounter = ReachedToCounter;

        movementController.StartMovement(currentLevelData, OnReachedToCounter, ClientPos.COUNTER);
        if(DataManager.Instance.Inventory.GetSlotCount() == 6)
        {
            UIManager.Instance.DeactivateSaveButton();
        }
        else
        {
            UIManager.Instance.ActivateSaveButton();
        }
    }

    private void ReachedToCounter()
    {
//        Debug.LogWarning("I am Reached fuckeer   " +currentLevelData.clientProfile.clientInformation.title);

        if (GameStateManager.GetCurrentState != GameState.Inventory)
        {
            StatsUI.Instance.SetBiodata(currentLevelData.clientProfile);
            UIManager.Instance.ActivateDecisionPanel();
        }
        isReachedToCounter = true;
        ChairEntry();
    }
    public void AcceptClient()
    {
        isAccept = true;
        isReject = false;
        isBackToBaseLoop = false;
        isReachedToCounter = false;

        UnityAction OnReachedFinish = ReachedToFinish;
        movementController.StartMovement(currentLevelData, ReachedToFinish, ClientPos.FINISH);
        StatsUI.Instance.DeactivateStatsPanel();
        ChairExit();

        Slot slot = new Slot();
        slot.savedProfileID = currentLevelData.clientProfile.clientID;
        DataManager.Instance.Inventory.AddSlotToSave(slot);

        DataManager.Instance.SaveData();

        StartCoroutine(GotoInventory());
    }

    IEnumerator GotoInventory()
    {
        LevelManager.Instance.IncreaseGameLevel();
        yield return new WaitForSeconds(2f);
        UIManager.Instance.DeactivateInventoryButton();
        UIManager.Instance.DeactivateLevelText();

        DataManager.Instance.LoadData();

        InventoryManager.Instance.ActivateInventory();
        InventoryManager.Instance.SetAllSlotWithSavedList(DataManager.Instance.Inventory.GetSavedSlotList());
        yield return new WaitForEndOfFrame();
        InventoryManager.Instance.HighlightCurrentSlot(currentLevelData.clientProfile);

        GameStateManager.RequestSwitch(GameState.Inventory);
    }

    private void ReachedToFinish()
    {
        Debug.LogWarning("Reached To Finish");

        StartCoroutine(WaitingRoutine());
    }

    public void MatchNow()
    {
        LevelManager.Instance.IncreaseGameLevel();
        DataManager.Instance.SetInvenotrySelectedProfile(currentLevelData.clientProfile);
        DataManager.Instance.SetInventoryProfileList(InventoryManager.Instance.clientProfiles);
        Gameplay.Instance.GotoMatchmakingScene();
    }

    IEnumerator WaitingRoutine()
    {
        Debug.LogError(GameStateManager.GetCurrentState);
        Gameplay.Instance.UpdateCurrentLevelData();

        if (GameStateManager.GetCurrentState == GameState.Inventory)
        {
            while (!isBackToBaseLoop)
            {
                yield return null;
            }
            LevelData levelData = Gameplay.Instance.GatherLevelData();
            StartEntryHallManager(levelData);
            UIManager.Instance.ActivateInventoryButton();
            UIManager.Instance.ActivateLevelText();
            isBackToBaseLoop = false;
        }
        else
        {
            Debug.Log("ELSE FROM NOW");
            LevelData levelData = Gameplay.Instance.GatherLevelData();
            StartEntryHallManager(levelData);
        }
    }

    public void RejectClient()
    {
        isAccept = false;
        isReject = true;

        isReachedToCounter = false;

        LevelManager.Instance.IncreaseGameLevel();

        UnityAction OnReachedFinish = ReachedToFinish;
        movementController.StartMovement(currentLevelData, ReachedToFinish, ClientPos.FINISH);
        StatsUI.Instance.DeactivateStatsPanel();
        ChairExit();
    }


    public void OpenEntryGate()
    {
        entryGateAnimator.SetTrigger(ENTRY);
    }
    public void CloseEntryGate()
    {
        entryGateAnimator.SetTrigger(EXIT);
    }


    public void OpenExitGate()
    {
        exitGameAnimator.SetTrigger(ENTRY);
    }
    public void CloseExitGate()
    {
        exitGameAnimator.SetTrigger(EXIT);
    }

    public void ChairEntry()
    {
        chairAnimator.SetTrigger(ENTRY);
    }
    public void ChairExit()
    {
        chairAnimator.SetTrigger(EXIT);
    }
}
