﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ScrollItem : MonoBehaviour
{
    public Button button;
    public Image proImage;
    public TextMeshProUGUI percentageText;
    public PPLoader ppLoader;

    UIManager uiManager;
    ClientProfile clientProfile;

    Color lerpColor;


    private void Start()
    {
        button.onClick.AddListener(delegate
        {
            uiManager.ClickButton(clientProfile);
        });
    }

    public void SetInfo(ClientProfile clientProfile,UIManager uiManager)
    {
        this.uiManager = uiManager;
        this.clientProfile = clientProfile;

        proImage.sprite = ppLoader.GetVisualProfilePicture(clientProfile.visualId);

        int A = ClientHelper.GetScore(Gameplay.Instance.currentLevelData.clientProfile.clientRequirement, clientProfile.clientInformation);
        int B = ClientHelper.GetScore(clientProfile.clientRequirement,Gameplay.Instance.currentLevelData.clientProfile.clientInformation);

        int addValue = (A + B) / 2;

        if(addValue <= 100 && addValue > 51)
        {
            lerpColor = Color.Lerp(Color.yellow, Color.green, (addValue / 100f));
        }
        else if (addValue < 51 && addValue >= 0)
        {
            lerpColor = Color.Lerp(Color.red, Color.yellow, (addValue / 100f));
        }

        percentageText.color = lerpColor;
        percentageText.text = (addValue.ToString()+"%");
    }

    public void Deactive()
    {
        gameObject.SetActive(false);
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }
}
