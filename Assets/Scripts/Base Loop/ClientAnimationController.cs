﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;
using UnityEngine.AI;

public class ClientAnimationController : MonoBehaviour
{
    public NavMeshAgent agent;
    public Animator animator;

    public PathCreator pathCreator;
    public float speed;
    public float distanceTravelled;

    private int WAITING = Animator.StringToHash("waiting");
    private int WALK = Animator.StringToHash("walk");
    private int GET_UP_SAD = Animator.StringToHash("getupsad");
    private int GET_UP_HAPPY = Animator.StringToHash("getuphappy");


    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {

    }

    public void MoveAgent(MovePoint movePoint, UnityEngine.Events.UnityAction unityAction, PathCreator entryPathCreator)
    {
        this.pathCreator = entryPathCreator;
        distanceTravelled = 0;
        PlayWalk();
        //agent.SetDestination(movePoint.pointTrans.position);
        StartCoroutine(MoveRoutine(movePoint,unityAction));
    }
    IEnumerator MoveRoutine(MovePoint movePoint, UnityEngine.Events.UnityAction unityAction)
    {
        bool isReached = false;

        while (!isReached)
        {
            float dist = Vector3.Distance(transform.position, movePoint.pointTrans.position);
            distanceTravelled += Time.deltaTime * speed;
            transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
            transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);

            if (dist < 0.1f)
            {
                //Arrived.

//                Debug.LogError("reached");
                isReached = true;
                PlayWaiting();
                unityAction.Invoke();
            }

            yield return null;
        }
    }

    internal IEnumerator CloseEntryDoor(MovePoint movePoint, Action unityAction)
    {
        bool isCloseGate = false;

        while (!isCloseGate)
        {
            float dist = Vector3.Distance(transform.position, movePoint.pointTrans.position);

            if (dist < 0.1f)
            {
                //Arrived.

//                Debug.LogError("reached");
                isCloseGate = true;

                unityAction.Invoke();
            }
            yield return null;
        }
    }

    internal IEnumerator CloseExitGate(MovePoint movePoint, Action p)
    {
        bool isCloseGate = false;

        while (!isCloseGate)
        {
            float dist = Vector3.Distance(transform.position, movePoint.pointTrans.position);

            if (dist < 0.1f)
            {
                //Arrived.

                
                isCloseGate = true;

                p.Invoke();
            }
            yield return null;
        }
    }

    internal IEnumerator OpenExitGate(MovePoint movePoint, Action unityAction)
    {
        bool isOpenGate = false;

        while (!isOpenGate)
        {
            float dist = Vector3.Distance(transform.position, movePoint.pointTrans.position);

            if (dist < 0.1f)
            {
                //Arrived.

                //Debug.LogError("reached");
                isOpenGate = true;

                unityAction.Invoke();
            }
            yield return null;
        }
    }

    internal IEnumerator OpenEntryDoor(MovePoint movePoint, Action unityAciton)
    {
        bool isOpenGate = false;
       
        while (!isOpenGate)
        {
            float dist = Vector3.Distance(transform.position, movePoint.pointTrans.position);

            if (dist < 0.1f)
            {
                //Arrived.

               
                isOpenGate = true;
                
                unityAciton.Invoke();
            }
            yield return null;
        }
    }

    private void PlayWaiting()
    {
        animator.SetTrigger(WAITING);
    }
    public void PlayWalk()
    {
        animator.SetTrigger(WALK);
    }

    public void GetUpSad()
    {
        animator.SetTrigger(GET_UP_SAD);
    }
    public void GetUpHappy()
    {
        animator.SetTrigger(GET_UP_HAPPY);
    }
}
