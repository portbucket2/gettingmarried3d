﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class StatsUI : MonoBehaviour
{
    public static StatsUI Instance;

    public GameObject biodataPanel;
    public GameObject expectationPanel;

    public List<TextMeshProUGUI> biodataTextList;
    public TextMeshProUGUI offerValueText;

    //public TextMeshProUGUI nameText;
    //public TextMeshProUGUI genderText;
    //public TextMeshProUGUI professionText;
    //public TextMeshProUGUI heightText;
    //public TextMeshProUGUI ageText;
    //public TextMeshProUGUI bodyText;

    public TextMeshProUGUI nameText;
    public TextMeshProUGUI lookingForInfo;
   

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    public void ActivateStatsPanel()
    {
        biodataPanel.SetActive(true);
        expectationPanel.SetActive(true);
    }

    public void DeactivateStatsPanel()
    {
        biodataPanel.SetActive(false);
        expectationPanel.SetActive(false);
    }

    public void SetBiodata(ClientProfile clientProfile)
    {
        biodataPanel.SetActive(true);

        List<string> stringList = new List<string>();
        stringList = ClientHelper.GetOptionalDisplayInfoFieldsEntryScene(clientProfile);

        List<float> fontSizeList = new List<float>();
        fontSizeList = ClientHelper.GetFontSizeList();

        //for (int i = 0; i < biodataTextList.Count; i++)
        //{
        //    biodataTextList[i].gameObject.SetActive(false);
        //}

        for (int i = 0; i < biodataTextList.Count; i++)
        {
            if(stringList.Count > i)
            {
                biodataTextList[i].gameObject.SetActive(true);
                biodataTextList[i].text = stringList[i];
               
                biodataTextList[i].fontSize = fontSizeList[i];
            }
            else
            {
                biodataTextList[i].gameObject.SetActive(false);
            }
           
        }
        offerValueText.text = "Offer: $" + clientProfile.initialCurrency.ToString();

        SetExpectation(clientProfile);
    }
    public void SetExpectation(ClientProfile clientProfile)
    {
        expectationPanel.SetActive(true);

        nameText.text = clientProfile.clientInformation.title;
        lookingForInfo.text = clientProfile.clientRequirement.title.ToString();
    }
   
}
