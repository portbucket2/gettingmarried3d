﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;

    public EntryHallManager entryHallManager;

    public LevelData currentLevelData;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        
    }
    private void Start()
    {

        if (DataManager.Instance.isBackFromMatchmaking)
        {
            UIManager.Instance.ResetEntry();
            StartGame();
        }
    }

    public void StartGame()
    {
        StartCoroutine(StartGameRoutine());
    }
    IEnumerator StartGameRoutine()
    {
       GameStateManager.RequestSwitch(GameState.BaseLoop);
       yield return null;
        currentLevelData = new LevelData();
        currentLevelData = GatherLevelData();
       entryHallManager.StartEntryHallManager(currentLevelData);
    }

    public LevelData GatherLevelData()
    {
        int t_CurrentLevel = LevelManager.Instance.GetCurrentLevel();
        LevelData levelData = new LevelData(t_CurrentLevel);
        return levelData;
    }

    public void UpdateCurrentLevelData()
    {
        currentLevelData = new LevelData();
        currentLevelData = GatherLevelData();
    }


    public void GotoGameplayScene()
    {
        SceneManager.LoadScene("Gameplay");
    }
    public void GotoMatchmakingScene()
    {
        SceneManager.LoadScene("Matchmaking");
    }

}
