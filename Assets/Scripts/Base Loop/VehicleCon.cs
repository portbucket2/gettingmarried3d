﻿using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class VehicleCon : MonoBehaviour
{
    public PathCreator pathCreator;
    public float speed;
    public float distanceTravelled;

    private void Update()
    {
        distanceTravelled += Time.deltaTime * speed;
        transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled);
        //transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled);
    }
}
