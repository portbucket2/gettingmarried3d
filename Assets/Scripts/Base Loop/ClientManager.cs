﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

public class ClientManager : MonoBehaviour
{
    public PathCreator entryPathCreator;
    public PathCreator exitPathCreator;

    public CharLoader charLoader;
    public ClientAnimationController clientController;


    //public List<ClientRef> clientObjectList;
    public List<MovePoint> movePointList;

    public void StartMovement(LevelData levelData, UnityEngine.Events.UnityAction unityAction, ClientPos t_ClientPos)
    {
        UIManager.Instance.LoadLevelNumber();
        StartCoroutine(MovementRoutine(levelData,unityAction,t_ClientPos));
    }
    IEnumerator MovementRoutine(LevelData levelData, UnityEngine.Events.UnityAction unityAction, ClientPos t_ClientPos)
    {
        yield return null;
        //ClientRef clientRef = new ClientRef();

        //foreach (var item in clientObjectList)
        //{
        //    if(levelData.id == item.clientID)
        //    {
        //        clientRef = item;
        //    }
        //}



        int visualID = levelData.clientProfile.visualId;
        charLoader.ActivateVisualChar(visualID);

        switch (t_ClientPos)
        {
            case ClientPos.INIT:
                break;
            case ClientPos.COUNTER:
                StartCoroutine(clientController.OpenEntryDoor(GetInitPoint(),()=> { EntryHallManager.Instance.OpenEntryGate(); }));
                StartCoroutine(clientController.CloseEntryDoor(GetEntryGateClosePoint(), () => { EntryHallManager.Instance.CloseEntryGate(); }));
                clientController.MoveAgent(GetCounterPoint(), unityAction,entryPathCreator);
                GetCounterPoint().isFill = true;
                break;
            case ClientPos.FINISH:
                yield return StartCoroutine(DecisionReactionRoutine());
                clientController.MoveAgent(GetFinishPoint(), unityAction,exitPathCreator);
                GetFinishPoint().isFill = true;
                StartCoroutine(clientController.OpenExitGate(GetExitGateOpenPoint(), () => { EntryHallManager.Instance.OpenExitGate(); }));
                StartCoroutine(clientController.CloseExitGate(GetExitGateClosePoint(), () => { EntryHallManager.Instance.CloseExitGate(); }));
                
                break;

            default:
                break;
        }
    }

    IEnumerator DecisionReactionRoutine()
    {
        if (EntryHallManager.Instance.isAccept)
        {
            clientController.GetUpHappy();
            yield return new WaitForSeconds(2f);
        }
        if (EntryHallManager.Instance.isReject)
        {
           clientController.GetUpSad();
            yield return new WaitForSeconds(1f);
        }
    }

    

    private MovePoint GetInitPoint()
    {
        return movePointList[0];
    }
    private MovePoint GetCounterPoint()
    {
        return movePointList[1];
    }
    private MovePoint GetFinishPoint()
    {
        return movePointList[2];
    }
    private MovePoint GetEntryGateClosePoint()
    {
        return movePointList[3];
    }
    private MovePoint GetExitGateOpenPoint()
    {
        return movePointList[4];
    }
    private MovePoint GetExitGateClosePoint()
    {
        return movePointList[5];
    }
}
