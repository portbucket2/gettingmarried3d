﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DropdownUI : MonoBehaviour
{
    public static DropdownUI Instance;

    public Dropdown dropdown1;
    public Dropdown dropdown2;

    public TextMeshProUGUI text1;
    public TextMeshProUGUI text2;
    public TextMeshProUGUI matchingText;

    public List<ClientProfile> clientList;

    public ClientProfile fixedProfile;
    public ClientProfile secondProfile;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        dropdown1.options.Clear();
        dropdown2.options.Clear();
    }

    public void SetDropdownDataDropdown(List<ClientProfile> clientList)
    {
        this.clientList = clientList;

        List<string> namelist = new List<string>();
        for (int i = 0; i < clientList.Count; i++)
        {
            namelist.Add(clientList[i].name);
        }
        dropdown1.AddOptions(namelist);
        dropdown2.AddOptions(namelist);
    }

    public void OnValueChangedDropDown1(int index)
    {
        text1.text = clientList[index].name;
        fixedProfile = clientList[index];
    }
    public void onValueChanged2(int index)
    {
        text2.text = clientList[index].name;
        secondProfile = clientList[index];
    }
   
}
