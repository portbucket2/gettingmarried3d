﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharGenerator : MonoBehaviour
{
    public static CharGenerator Instance;

    public List<ClientRef> charList;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void ActivateSelectedChar(int id)
    {
        foreach (var item in charList)
        {
            item.clientObject.SetActive(false);
        }

        foreach (var item in charList)
        {
            if(item.clientID == id)
            {
                item.clientObject.SetActive(true);
            }
        }
    }

}
