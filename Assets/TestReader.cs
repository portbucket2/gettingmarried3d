﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FRIA;

public class TestReader : MonoBehaviour
{
    public List<ClientInfo> testClassList = new List<ClientInfo>();
    public TextAsset peelData;

    private void Start()
    {
        CSVReader.Parse_toClassWithBasicConstructor(testClassList, peelData);
    }
}